import "./App.css";
import Main from "./BTState/Main";

function App() {
  return (
    <div>
      <Main />
    </div>
  );
}

export default App;

import React from "react";
import "./SCSS/list.scss";
import Item from "./Item";

const ListGlass = (props) => {
  const { data, handleGlasses } = props;
  return (
    <div className="ListGlass container grid grid-cols-6">
      {data.map((item) => {
        return (
          <Item data={item} handleGlasses={handleGlasses} key={item.id}></Item>
        );
      })}
    </div>
  );
};

export default ListGlass;

import React from "react";
import "./SCSS/main.scss";
import GlassDetail from "./GlassDetail";
import ListGlass from "./ListGlass";
import data from "./data.json";
import { useState } from "react";

const Main = () => {
  const [glasses, setGlasses] = useState(data[6]);
  const handleGlasses = (item) => {
    setGlasses(item);
  };
  return (
    <div className="Main h-screen">
      <div className="layout">
        <div className="top w-screen text-center py-10">
          <h1 className="text-2xl font-semibold">TRY GLASSES APP ONLINE</h1>
        </div>
        <GlassDetail glasses={glasses}></GlassDetail>
        <ListGlass data={data} handleGlasses={handleGlasses}></ListGlass>
      </div>
    </div>
  );
};

export default Main;
